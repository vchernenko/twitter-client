package com.chernenkovit.base.domain.interactor

import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.presentation.utils.DevUtils
import io.reactivex.Single
import io.reactivex.annotations.CheckReturnValue
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCaseWithoutParams<T> constructor(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) {

  private val disposables = CompositeDisposable()

  /**
   * Builds a [Single] which will be used when the current [SingleUseCase] is executed.
   */
  protected abstract fun buildUseCaseObservable(): Single<T>

  /**
   * Executes the current use case.
   */
  open fun execute(singleObserver: DisposableSingleObserver<T>) {
    val single = this.buildUseCaseObservable()
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.scheduler) as Single<T>
    addDisposable(single.subscribeWith(singleObserver))
  }

  /**
   * Executes the current state but with lambdas instead of callback
   */

  @CheckReturnValue
  @JvmOverloads
  fun executeLambdas(
      onSuccess: (T) -> Unit = {},
      onFailure: (Throwable) -> Unit = {},
      autoDisposable: Boolean = false
  ): Disposable {
    var disposable: Disposable? = null
    disposable = buildUseCaseObservable()
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.scheduler)
        .subscribe(
            {
              onSuccess(it)
              if (autoDisposable) {
                disposable?.dispose()
              }
            },
            {
              DevUtils.log(it)
              onFailure(it)
              if (autoDisposable) {
                disposable?.dispose()
              }
            }
        )
    return disposable
  }

  /**
   * Executes the current use case without subscription.
   * Handle it by yourself.
   */
  fun executeWithoutSubscription(): Single<T> {
    return buildUseCaseObservable()
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.scheduler)
  }

  /**
   * Executes the current use case without subscription.
   * Handle it by yourself.
   */
  fun executeOriginWithoutSubscription(): Single<T> {
    return buildUseCaseObservable()
  }

  /**
   * Dispose from current [CompositeDisposable].
   */
  fun dispose() {
    if (!disposables.isDisposed) {
      disposables.dispose()
    }
  }

  /**
   * Dispose from current [CompositeDisposable].
   */
  protected fun addDisposable(disposable: Disposable) {
    disposables.add(disposable)
  }
}