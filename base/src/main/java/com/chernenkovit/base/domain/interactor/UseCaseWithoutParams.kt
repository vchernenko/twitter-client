package com.chernenkovit.base.domain.interactor

import com.chernenkovit.base.data.network.error.Failure
import com.chernenkovit.base.domain.functional.Either
import kotlinx.coroutines.*

abstract class UseCaseWithoutParams<out Type> where Type : Any {
    private val mainJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + mainJob)

    abstract suspend fun run(): Either<Failure, Type>

    operator fun invoke(onResult: (Either<Failure, Type>) -> Unit = {}) {
        uiScope.launch {
            val job = async(Dispatchers.IO) { run() }
            onResult(job.await())
        }
    }
}