

package com.chernenkovit.base.domain.interactor


import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.presentation.utils.DevUtils
import io.reactivex.Single
import io.reactivex.annotations.CheckReturnValue
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


/**
 * Abstract class for a UseCase that returns an instance of a [Single].
 */
abstract class SingleUseCase<T, in Params> constructor(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) {

  private val disposables = CompositeDisposable()

  /**
   * Builds a [Single] which will be used when the current [SingleUseCase] is executed.
   */
  protected abstract fun buildUseCaseObservable(params: Params): Single<T>

  /**
   * Executes the current use case.
   */
  open fun execute(singleObserver: DisposableSingleObserver<T>, params: Params) {
    val single = this.buildUseCaseObservable(params)
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.scheduler) as Single<T>
    addDisposable(single.subscribeWith(singleObserver))
  }

  /**
   * Executes the current use case without subscription.
   * Handle it by yourself.
   */
  fun executeWithoutSubscription(params: Params): Single<T> {
    return buildUseCaseObservable(params)
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.scheduler)
  }

  /**
   * Executes the current state but with lambdas instead of callback
   */

  @CheckReturnValue
  @JvmOverloads
  fun executeLambdas(params: Params,
      onSuccess: (T) -> Unit = {},
      onFailure: (Throwable) -> Unit = {},
      autoDisposable: Boolean = false
  ): Disposable {
    var disposable: Disposable? = null
    disposable = buildUseCaseObservable(params)
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.scheduler)
        .subscribe(
            {
              onSuccess(it)
              if (autoDisposable) {
                disposable?.dispose()
              }
            },
            {
              DevUtils.log(it)
              onFailure(it)
              if (autoDisposable) {
                disposable?.dispose()
              }
            }
        )
    addDisposable(disposable)
    return disposable
  }

  /**
   * Dispose from current [CompositeDisposable].
   */
  fun dispose() {
    if (!disposables.isDisposed) {
      disposables.dispose()
    }
  }

  /**
   * Dispose from current [CompositeDisposable].
   */
  protected fun addDisposable(disposable: Disposable) {
    disposables.add(disposable)
  }
}