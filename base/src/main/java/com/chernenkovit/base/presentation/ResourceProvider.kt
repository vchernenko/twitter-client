package com.chernenkovit.base.presentation

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import androidx.annotation.*
import androidx.core.content.ContextCompat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourcesProvider @Inject constructor(private val context: Context) {
    private val resources: Resources = context.resources

    val locale: Locale
        get() = resources.configuration.locale

    fun getString(@StringRes textId: Int): String {
        return context.getString(textId)
    }

    fun getString(@StringRes textId: Int, vararg args: Any?): String {
        return context.getString(textId, *args)
    }

    fun getStringArray(@ArrayRes arrayId: Int): Array<String> {
        return context.resources.getStringArray(arrayId)
    }

    fun getQuantityString(@PluralsRes id: Int, quantity: Int): String {
        return resources.getQuantityString(id, quantity)
    }

    fun getQuantityString(@PluralsRes id: Int, quantity: Int, vararg args: Any): String {
        return resources.getQuantityString(id, quantity, *args)
    }

    fun getDimensionPixelSize(@DimenRes id: Int): Int {
        return resources.getDimensionPixelSize(id)
    }

    fun getDimension(@DimenRes id: Int): Float {
        return resources.getDimension(id)
    }

    fun getDimensionPixelOffset(@DimenRes id: Int): Int {
        return resources.getDimensionPixelOffset(id)
    }

    fun getColor(@ColorRes colorId: Int): Int {
        return ContextCompat.getColor(context, colorId)
    }

    fun getDrawable(drawableId: Int): Drawable? {
        return ContextCompat.getDrawable(context, drawableId)
    }

    fun getResources(): Resources {
        return resources
    }
}
