package com.chernenkovit.base.presentation.di.component

import android.content.Context
import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.storage.PreferenceUtils
import com.chernenkovit.base.data.storage.TwitterTokenProvider
import com.chernenkovit.base.data.network.NetworkStateProvider
import com.chernenkovit.base.presentation.navigator.Navigator
import okhttp3.OkHttpClient
import retrofit2.Retrofit

interface BaseAppComponentApi {
    fun retrofit(): Retrofit
    fun context(): Context
    fun preferenceUtils(): PreferenceUtils
    fun okHttpClient(): OkHttpClient
    fun networkHandler(): NetworkStateProvider
    fun twitterAuth(): TwitterAuth
    fun twitterTokenProvider(): TwitterTokenProvider
    fun threadExecutor(): ThreadExecutor
    fun postExecutionThread(): PostExecutionThread
    fun navigator(): Navigator
}