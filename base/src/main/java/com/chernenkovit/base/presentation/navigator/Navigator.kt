package com.chernenkovit.base.presentation.navigator

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import javax.inject.Inject
import javax.inject.Singleton

//TODO: implement RouteActivity in base module
@Singleton
class Navigator @Inject constructor() {
    fun goToTimeline(context: Context) {
        val intent = Intent()
        intent.component =
            ComponentName(context, Class.forName("com.chernenkovit.timeline.MainActivity"))
        context.startActivity(intent)
    }
}