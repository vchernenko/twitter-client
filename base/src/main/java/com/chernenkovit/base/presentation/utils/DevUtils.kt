package com.chernenkovit.base.presentation.utils

import android.util.Log
import com.chernenkovit.base.BuildConfig

object DevUtils {
    private const val TAG = "TwitterClient"

    fun log(text: String) {
        if (isDebug()) Log.e(TAG, text)
    }
    fun log(t: Throwable) {
        if (isDebug()) t.printStackTrace()
    }

    fun isDebug() = BuildConfig.DEBUG
}