package com.chernenkovit.base.presentation.di

interface HasComponent<C> {
    fun initializeInjector()
    fun getComponent(): C
}