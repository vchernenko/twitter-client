package com.chernenkovit.base.presentation.mvp

import androidx.annotation.CallSuper
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V : MvpView> : MvpBasePresenter<V>() {

    protected var subscriptions: CompositeDisposable? = null

    @CallSuper
    override fun attachView(view: V) {
        super.attachView(view)
        if (subscriptions == null) {
            subscriptions = CompositeDisposable()
        }
    }

    @CallSuper
    override fun detachView() {
        super.detachView()
        subscriptions?.clear()
    }

    @CallSuper
    override fun destroy() {
        super.destroy()
        subscriptions?.dispose()
    }
}