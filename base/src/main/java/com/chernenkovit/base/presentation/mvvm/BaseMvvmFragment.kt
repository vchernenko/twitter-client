package com.chernenkovit.base.presentation.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.chernenkovit.base.presentation.di.BaseComponent
import com.chernenkovit.base.presentation.di.HasComponent
import com.chernenkovit.base.presentation.extension.AndroidJob
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

abstract class BaseMvvmFragment<C : BaseComponent>(@LayoutRes val layoutRes: Int) :
    Fragment(), CoroutineScope, HasComponent<C> {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewComponent: C
    private val job = AndroidJob(lifecycle)
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    protected fun toast(message: String) =
        activity?.let {
            Toast.makeText(it, message, Toast.LENGTH_SHORT).show()
        }

    override fun getComponent(): C = viewComponent
}