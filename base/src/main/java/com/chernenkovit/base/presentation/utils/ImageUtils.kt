package com.chernenkovit.base.presentation.utils

import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.chernenkovit.base.R

object ImageUtils {
    fun loadPhoto(imageView: ImageView, resId: Int) {
        load(imageView, resId)
    }

    fun loadPhoto(imageView: ImageView, uri: Uri) {
        load(imageView, uri)
    }

    fun loadPhoto(imageView: ImageView, path: String) {
        load(imageView, path)
    }

    private fun load(imageView: ImageView, path: Any) {
        Glide.with(imageView)
            .load(path)
            .apply(
                RequestOptions
                    .diskCacheStrategyOf(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .circleCrop()
                    .placeholder(R.drawable.ic_man)
            )
            .transition(DrawableTransitionOptions.withCrossFade(300))
            .thumbnail(0.1f)
            .into(imageView)
            .clearOnDetach()
    }
}