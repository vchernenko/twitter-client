package com.chernenkovit.base.presentation.di.component

import com.chernenkovit.base.TwitterClientApp
import com.chernenkovit.base.data.di.ExecutorsModule
import com.chernenkovit.base.data.di.NetworkModule
import com.chernenkovit.base.presentation.di.BaseComponent
import com.chernenkovit.base.presentation.di.module.ContextModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, NetworkModule::class, ExecutorsModule::class])
interface BaseAppComponent : BaseComponent, BaseAppComponentApi {

    fun inject(app: TwitterClientApp)

    companion object {
        @Volatile
        private lateinit var baseAppComponent: BaseAppComponent

        fun init(baseAppComponent: BaseAppComponent) {
            if (this::baseAppComponent.isInitialized) {
                throw IllegalArgumentException("BaseAppComponent is already initialized.")
            }
            Companion.baseAppComponent = baseAppComponent
        }

        fun get(): BaseAppComponent {
            if (!this::baseAppComponent.isInitialized) {
                throw NullPointerException("BaseAppComponent is not initialized yet. Call init first.")
            }
            return baseAppComponent
        }
    }
}