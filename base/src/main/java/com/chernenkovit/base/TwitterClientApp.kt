package com.chernenkovit.base

import android.app.Application
import com.chernenkovit.base.presentation.di.HasComponent
import com.chernenkovit.base.presentation.di.component.BaseAppComponent
import com.chernenkovit.base.presentation.di.component.DaggerBaseAppComponent
import com.chernenkovit.base.presentation.di.module.ContextModule
import com.facebook.stetho.Stetho

open class TwitterClientApp : Application(), HasComponent<BaseAppComponent> {

    private lateinit var appComponent: BaseAppComponent

    init {
        initializeInjector()
        getComponent().inject(this)
    }

    override fun onCreate() {
        super.onCreate()
        // Create an InitializerBuilder
        val initializerBuilder = Stetho.newInitializerBuilder(this)

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
            Stetho.defaultInspectorModulesProvider(this)
        )

        // Enable command line interface
        initializerBuilder.enableDumpapp(
            Stetho.defaultDumperPluginsProvider(this)
        )

        // Use the InitializerBuilder to generate an Initializer
        val initializer = initializerBuilder.build()

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer)
    }

    override fun initializeInjector() {
        this.appComponent = DaggerBaseAppComponent
            .builder()
            .contextModule(ContextModule(this))
            .build()
        BaseAppComponent.init(appComponent)
    }

    override fun getComponent(): BaseAppComponent = appComponent
}