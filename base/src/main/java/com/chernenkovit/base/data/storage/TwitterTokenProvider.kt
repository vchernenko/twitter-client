package com.chernenkovit.base.data.storage

import com.chernenkovit.base.data.model.TwitterToken
import javax.inject.Singleton

@Singleton
class TwitterTokenProvider {

    var twitterToken: TwitterToken? = null
}