package com.chernenkovit.base.data.network.error

object NetworkStatusCode {
    const val STATUS_OK = 1
    const val UNKNOWN_ERROR_CODE = -1

    fun getErrorMessage(code: Int): String =
        when (code) {
            STATUS_OK -> "Success"
            else -> "Error"
        }
}