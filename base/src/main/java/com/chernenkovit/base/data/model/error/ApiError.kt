package com.chernenkovit.base.data.model.error

/** API error.  */
interface ApiError {

    /** API error code. Return [.INVALID_ERROR] if code can't be parsed.  */
    val code: Int
    /** API error message.  */
    val message: String?

    companion object {
        val INVALID_ERROR = 0
    }
}