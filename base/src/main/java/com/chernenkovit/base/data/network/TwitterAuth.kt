package com.chernenkovit.base.data.network

import android.util.Base64
import javax.inject.Singleton

/** Twitter authorization.  */
@Singleton
class TwitterAuth(
    /** API client.  */
    val client: String,
    /** API secret.  */
    val secret: String
) {
    override fun toString(): String {
        val credentials = "$client:$secret"
        return "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
    }
}
