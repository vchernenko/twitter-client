package com.chernenkovit.base.data.model

import com.google.gson.annotations.SerializedName

/** TwitterToken response.  */
class TwitterToken {
    /** TwitterToken type.  */
    @SerializedName("token_type")
    var type: String = ""
    /** Access token.  */
    @SerializedName("access_token")
    var token: String = ""

    constructor() {
        // for GSON
    }

    constructor(type: String, token: String) {
        this.type = type
        this.token = token
    }

    override fun toString(): String {
        return "$type $token"
    }
}
