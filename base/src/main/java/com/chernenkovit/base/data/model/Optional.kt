package com.chernenkovit.base.data.model

data class Optional<T>(val value: T?)

fun <T> T?.asOptional() = Optional(this)