package com.chernenkovit.base.data.storage

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.chernenkovit.base.BuildConfig
import javax.inject.Inject
import javax.inject.Singleton

const val PREFERENCE_NAME = BuildConfig.APPLICATION_ID + BuildConfig.FLAVOR

@Singleton
class PreferenceUtils @Inject constructor(context: Context) {

    private val prefs = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE)

}