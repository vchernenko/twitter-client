package com.chernenkovit.base.data.storage

import com.chernenkovit.base.data.network.error.Failure
import com.chernenkovit.base.data.network.error.NetworkStatusCode
import com.chernenkovit.base.data.network.error.NetworkStatusCode.UNKNOWN_ERROR_CODE
import com.chernenkovit.base.domain.functional.Either
import retrofit2.Call

open class MvvmBaseRemoteDataStore {
    protected fun <R> request(call: Call<R>, default: R): Either<Failure, R> {
        return try {
            val response = call.execute()
            when (response.isSuccessful) {
                true -> Either.Right(response.body() ?: default)
                false -> Either.Left(Failure.ServerError(response.code(), response.message()))
            }
        } catch (exception: Throwable) {
            Either.Left(
                Failure.ServerError(
                    UNKNOWN_ERROR_CODE,
                    NetworkStatusCode.getErrorMessage(UNKNOWN_ERROR_CODE)
                )
            )
        }
    }
}