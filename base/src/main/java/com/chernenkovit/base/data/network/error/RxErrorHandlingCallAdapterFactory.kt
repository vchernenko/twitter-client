package com.chernenkovit.base.data.network.error


import com.chernenkovit.base.data.model.error.ApiError
import com.chernenkovit.base.data.model.error.ApiErrorException
import com.chernenkovit.base.data.utils.NetworkUtils
import io.reactivex.*
import io.reactivex.functions.Function
import okhttp3.ResponseBody
import retrofit2.*
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type
import java.net.SocketTimeoutException


/**
 * Custom call adapter used to handle and parse API errors.
 *
 * @param <E> error type
</E> */
class RxErrorHandlingCallAdapterFactory<E : ApiError> private constructor(
    /** Error type.  */
    private val clazz: Class<E>) : CallAdapter.Factory() {
    /** Overridden call adapter.  */
    private val original = RxJava2CallAdapterFactory.create()

    override fun get(returnType: Type, annotations: Array<Annotation>,
                     retrofit: Retrofit
    ): CallAdapter<*, *>? {
        val callAdapterDelegate = original.get(returnType, annotations, retrofit)
        if (callAdapterDelegate != null) {
            val rawType = getRawType(returnType)
            if (rawType == Single::class.java) {
                return SingleCallAdapter(callAdapterDelegate as CallAdapter<out Any, Single<*>>, retrofit, annotations)
            }
            if (rawType == Observable::class.java) {
                return ObservableCallAdapter(callAdapterDelegate as CallAdapter<out Any, Observable<*>>, retrofit, annotations)
            }

            if (rawType == Maybe::class.java) {
                return MaybeCallAdapter(callAdapterDelegate as CallAdapter<out Any, Maybe<*>>, retrofit, annotations)
            }

            if (rawType == Completable::class.java) {
                return CompletableCallAdapter(callAdapterDelegate as CallAdapter<out Any, Completable>, retrofit, annotations)
            }
        }

        return callAdapterDelegate
    }

    companion object {
        fun <E : ApiError> create(clazz: Class<E>): CallAdapter.Factory {
            return RxErrorHandlingCallAdapterFactory(clazz)
        }
    }

    private inner class SingleCallAdapter<RESPONSE>(
        private val delegate: CallAdapter<RESPONSE, Single<*>>,
        private val retrofit: Retrofit,
        private val annotations: Array<Annotation>) : CallAdapter<RESPONSE, Single<*>> {
        override fun adapt(call: Call<RESPONSE>): Single<*> {
            return delegate.adapt(call)
                .onErrorResumeNext {
                    when (it) {
                        is HttpException -> handleApiError(it, retrofit, annotations)
                        is IOException -> handleNetworkError(it)
                        else -> Single.error<Nothing>(
                            ApiErrorException(ApiErrorException.ERROR_GENERAL, it.message)
                        )
                    }
                }
        }

        override fun responseType(): Type {
            return delegate.responseType()
        }

        private fun handleApiError(ex: HttpException, retrofit: Retrofit,
                                   annotations: Array<Annotation>): SingleSource<Nothing> {
            val (code, message) = getApiCodeAndMessage(retrofit, annotations, ex)
            return Single.error<Nothing>(ApiErrorException(code, message))
        }

        private fun handleNetworkError(ex: IOException): Single<Nothing> {
            val (code, message) = getNetworkCodeAndMessage(ex)
            return Single.error<Nothing>(ApiErrorException(code, message))
        }
    }

    private inner class ObservableCallAdapter<RESPONSE>(
        private val delegate: CallAdapter<RESPONSE, Observable<*>>,
        private val retrofit: Retrofit,
        private val annotations: Array<Annotation>) : CallAdapter<RESPONSE, Observable<*>> {
        override fun adapt(call: Call<RESPONSE>): Observable<*> {
            return delegate.adapt(call)
                .onErrorResumeNext(Function {
                    when (it) {
                        is HttpException -> handleApiError(it, retrofit, annotations)
                        is IOException -> handleNetworkError(it)
                        else -> Observable.error<Nothing>(
                            ApiErrorException(ApiErrorException.ERROR_GENERAL, it.message))
                    }
                })
        }

        override fun responseType(): Type {
            return delegate.responseType()
        }

        private fun handleApiError(ex: HttpException, retrofit: Retrofit,
                                   annotations: Array<Annotation>): Observable<Nothing> {
            val (code, message) = getApiCodeAndMessage(retrofit, annotations, ex)
            return Observable.error<Nothing>(ApiErrorException(code, message))
        }

        private fun handleNetworkError(ex: IOException): Observable<Nothing> {
            val (code, message) = getNetworkCodeAndMessage(ex)
            return Observable.error<Nothing>(ApiErrorException(code, message))
        }
    }

    private inner class MaybeCallAdapter<RESPONSE>(
        private val delegate: CallAdapter<RESPONSE, Maybe<*>>,
        private val retrofit: Retrofit,
        private val annotations: Array<Annotation>) : CallAdapter<RESPONSE, Maybe<*>> {
        override fun adapt(call: Call<RESPONSE>): Maybe<*> {
            return delegate.adapt(call)
                .onErrorResumeNext(Function {
                    when (it) {
                        is HttpException -> handleApiError(it, retrofit, annotations)
                        is IOException -> handleNetworkError(it)
                        else -> Maybe.error<Nothing>(
                            ApiErrorException(ApiErrorException.ERROR_GENERAL, it.message))
                    }
                })
        }

        override fun responseType(): Type {
            return delegate.responseType()
        }

        private fun handleApiError(ex: HttpException, retrofit: Retrofit,
                                   annotations: Array<Annotation>): Maybe<Nothing> {
            val (code, message) = getApiCodeAndMessage(retrofit, annotations, ex)
            return Maybe.error<Nothing>(ApiErrorException(code, message))
        }

        private fun handleNetworkError(ex: IOException): Maybe<Nothing> {
            val (code, message) = getNetworkCodeAndMessage(ex)
            return Maybe.error<Nothing>(ApiErrorException(code, message))
        }
    }

    private inner class CompletableCallAdapter<RESPONSE>(
        private val delegate: CallAdapter<RESPONSE, Completable>,
        private val retrofit: Retrofit,
        private val annotations: Array<Annotation>) : CallAdapter<RESPONSE, Completable> {
        override fun adapt(call: Call<RESPONSE>): Completable {
            return delegate.adapt(call)
                .onErrorResumeNext {
                    when (it) {
                        is HttpException -> handleApiError(it, retrofit, annotations)
                        is IOException -> handleNetworkError(it)
                        else -> Completable.error(
                            ApiErrorException(ApiErrorException.ERROR_GENERAL, it.message))
                    }
                }
        }

        override fun responseType(): Type {
            return delegate.responseType()
        }

        private fun handleApiError(ex: HttpException, retrofit: Retrofit,
                                   annotations: Array<Annotation>): Completable {
            val (code, message) = getApiCodeAndMessage(retrofit, annotations, ex)
            return Completable.error(ApiErrorException(code, message))
        }

        private fun handleNetworkError(ex: IOException): Completable {
            val (code, message) = getNetworkCodeAndMessage(ex)
            return Completable.error(ApiErrorException(code, message))
        }
    }

    private fun getNetworkCodeAndMessage(ex: IOException): Pair<Int, String?> {
        val code = if (ex is SocketTimeoutException) {
            ApiErrorException.ERROR_TIMEOUT
        } else {
            ApiErrorException.ERROR_NO_INTERNET
        }
        val message = ex.message

        return code to message
    }

    private fun getApiCodeAndMessage(retrofit: Retrofit, annotations: Array<Annotation>,
                                     ex: HttpException): Pair<Int, String?> {
        val body = ex.response().errorBody()
        val code: Int
        val message: String?
        // Only codes from 400 to 499 are treated as client errors
        if (NetworkUtils.isClientError(ex.code()) && body != null) {
            var error: ApiError? = null
            try {
                val converter: Converter<ResponseBody, ApiError> = retrofit.responseBodyConverter(clazz,
                    annotations)
                error = converter.convert(body)
                //CHECKSTYLE:OFF
            } catch (e: IOException) {
                //CHECKSTYLE:ON
                e.printStackTrace()
            }

            if (error != null) {
                val parsedCode = error.code
                // sometimes code can't be parsed, so we use exception code in this case
                code = if (parsedCode != ApiError.INVALID_ERROR) {
                    error.code
                } else {
                    ex.code()
                }
                message = error.message
            } else {
                code = ex.code()
                message = ex.message
            }
        } else {
            // any other error are server side errors client can't handle specifically
            code = ex.code()
            message = ex.message
        }

        return code to message
    }
}
