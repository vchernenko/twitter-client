package com.chernenkovit.base.data.model.error


/** API call error exception.  */
class ApiErrorException : RuntimeException {

    /** TwitterApiError code.  */
    val code: Int

    constructor(code: Int) {
        this.code = code
    }

    constructor(code: Int, t: Throwable) : super(t) {
        this.code = code
    }

    constructor(code: Int, message: String?) : super(message) {
        this.code = code
    }

    companion object {
        /** General error code.  */
        val ERROR_GENERAL = -1
        /** No internet connection error code.  */
        val ERROR_NO_INTERNET = -2
        /** Connection timeout error code.  */
        val ERROR_TIMEOUT = -3
    }
}