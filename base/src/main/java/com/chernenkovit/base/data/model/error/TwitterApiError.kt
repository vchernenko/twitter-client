package com.chernenkovit.base.data.model.error

import com.google.gson.annotations.SerializedName


/** Base response.  */
class TwitterApiError : ApiError {
    // Case #1 when API returns array of errors
    /** Errors.  */
    @SerializedName("errors")
    private val errors: List<Error>? = null
    // Case #2 when API returns error itself
    /** TwitterApiError.  */
    @SerializedName("error")
    private val error: String? = null
    /** Request.  */
    @SerializedName("request")
    private val request: String? = null

    override val code: Int
        get() {
            if (error != null) {
                return 0
            }
            return if (errors!!.isNotEmpty()) {
                errors[0].code
            } else 0
        }

    override val message: String?
        get() {
            if (error != null) {
                return error
            }
            return if (errors!!.isNotEmpty()) {
                errors[0].message
            } else null
        }

    /** TwitterApiError response.  */
    class Error {
        /** TwitterApiError code.  */
        @SerializedName("code")
        val code: Int = 0
        /** TwitterApiError message.  */
        @SerializedName("message")
        val message: String? = null
        /** TwitterApiError label.  */
        @SerializedName("label")
        val label: String? = null
    }

}
