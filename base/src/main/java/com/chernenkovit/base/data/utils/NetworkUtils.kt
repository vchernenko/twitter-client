package com.chernenkovit.base.data.utils

/** Network utilities.  */
object NetworkUtils {
    /** Client error range.  */
    private val CLIENT_ERROR = 400
    /** Server error range.  */
    private val SERVER_ERROR = 500

    /**
     * Checks whether HTTP code related to client error.
     * @param code HTTP code
     * @return `true` if HTTP code it related to client error
     */
    fun isClientError(code: Int): Boolean {
        return code in CLIENT_ERROR until SERVER_ERROR
    }

    /**
     * Checks whether HTTP code related to server error.
     * @param code HTTP code
     * @return `true` if HTTP code it related to server error
     */
    fun isServerError(code: Int): Boolean {
        return code >= SERVER_ERROR
    }

}
