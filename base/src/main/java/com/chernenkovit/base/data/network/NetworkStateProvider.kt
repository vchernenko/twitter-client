package com.chernenkovit.base.data.network

import android.content.Context
import com.chernenkovit.base.presentation.extension.isConnected
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Injectable class which returns information about the network connection state.
 */
@Singleton
class NetworkStateProvider @Inject constructor(private val context: Context) {
    val isOnline get() = context.isConnected
}