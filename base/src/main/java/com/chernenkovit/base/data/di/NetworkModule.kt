package com.chernenkovit.base.data.di

import com.chernenkovit.base.BuildConfig
import com.chernenkovit.base.data.model.error.TwitterApiError
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.network.error.RxErrorHandlingCallAdapterFactory
import com.chernenkovit.base.data.storage.TwitterTokenProvider
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
abstract class NetworkModule {

    @Module
    companion object {
        @JvmStatic
        @Singleton
        @Provides
        fun provideRetrofit(httpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
            val retrofitBuilder = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create(TwitterApiError::class.java))
                .client(httpClient)
            return retrofitBuilder.build()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideGsonConverterFactory(): GsonConverterFactory {
            val gson = GsonBuilder()
                .setDateFormat(TWITTER_CREATE_AT_DATE_PATTERN)
                .create()
            return GsonConverterFactory.create(gson)
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideHttpClient(twitterTokenProvider: TwitterTokenProvider): OkHttpClient {
            val interceptor1 = Interceptor { chain ->

                var request = chain.request()

                if (request.header(AUTH_HEADER) == null && twitterTokenProvider.twitterToken!= null) {
                    request = chain.request()
                        .newBuilder()
                        .addHeader(AUTH_HEADER, twitterTokenProvider.twitterToken.toString())
                        .build()
                }

                var s = bodyToString(chain.request())
                chain.proceed(request)
            }
            val builder = OkHttpClient.Builder().readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(interceptor1)
                .addNetworkInterceptor(StethoInterceptor())
            return builder.build()
        }

        private fun bodyToString(request: Request?): String {
            if (request == null) {
                return ""
            }
            try {
                val copy: Request = request.newBuilder().build()
                val buffer = Buffer()
                if (copy.body() == null) {
                    return ""
                }
                copy.body()!!.writeTo(buffer)
                return buffer.readUtf8()
            } catch (e: IOException) {
                return "Failed to convert request"
            }

        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideTwitterAuth(): TwitterAuth {
            return TwitterAuth(BuildConfig.API_CLIENT, BuildConfig.API_SECRET)
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideTwitterTokenProvider(): TwitterTokenProvider {
            return TwitterTokenProvider()
        }

        /** Twitter "create_at" date pattern.  */
        const val TWITTER_CREATE_AT_DATE_PATTERN = "EEE MMM dd HH:mm:ss Z yyyy"
        const val AUTH_HEADER = "Authorization"
    }
}