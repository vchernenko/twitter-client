package com.chernenkovit.base.data.di

import com.chernenkovit.base.data.executor.JobExecutor
import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.data.executor.UiThread
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ExecutorsModule {

    @Singleton
    @Binds
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor

    @Binds
    @Singleton
    fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread
}
