package com.chernenkovit.feature_auth.data.repository

import com.chernenkovit.base.data.model.TwitterToken
import com.chernenkovit.base.data.network.NetworkStateProvider
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.network.error.Failure
import com.chernenkovit.base.data.network.error.NetworkStatusCode
import com.chernenkovit.base.data.storage.PreferenceUtils
import com.chernenkovit.base.domain.functional.Either
import com.chernenkovit.base.presentation.di.scope.PerFeature
import com.chernenkovit.feature_auth.data.net.AuthService
import com.chernenkovit.feature_auth.domain.repository.AuthRepository
import retrofit2.Call
import javax.inject.Inject

@PerFeature
class AuthRepositoryImpl @Inject constructor(
    private val authService: AuthService,
    private val preferenceUtils: PreferenceUtils,
    private val networkHandler: NetworkStateProvider,
    private val twitterAuth: TwitterAuth

) : AuthRepository {

    override fun getGuestToken(): Either<Failure, TwitterToken> =
        request(authService.getGuestToken(twitterAuth.toString()), TwitterToken())

    private fun <R> request(call: Call<R>, default: R): Either<Failure, R> {
        if (!networkHandler.isOnline) return Either.Left(Failure.NetworkConnection)
        return try {
            val response = call.execute()
            when (response.isSuccessful) {
                true -> Either.Right(response.body() ?: default)
                false -> Either.Left(
                    Failure.ServerError(
                        response.code(),
                        NetworkStatusCode.getErrorMessage(response.code())
                    )
                )
            }
        } catch (exception: Throwable) {
            Either.Left(Failure.ServerError(NetworkStatusCode.UNKNOWN_ERROR_CODE, ""))
        }
    }
}