package com.chernenkovit.feature_auth.data.net

import com.chernenkovit.base.data.model.TwitterToken
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject


class AuthService @Inject constructor(retrofit: Retrofit) : AuthApi {

    private val authApi by lazy { retrofit.create(AuthApi::class.java) }

    override fun getGuestToken(credentials: String): Call<TwitterToken> = authApi.getGuestToken(credentials)

}