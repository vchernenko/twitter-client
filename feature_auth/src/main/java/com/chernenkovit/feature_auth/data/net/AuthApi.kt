package com.chernenkovit.feature_auth.data.net

import com.chernenkovit.base.data.model.TwitterToken
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

const val GUEST_TOKEN_ENDPOINT = "oauth2/token?grant_type=client_credentials"

interface AuthApi {

    @POST(GUEST_TOKEN_ENDPOINT)
    fun getGuestToken(@Header("Authorization") credentials: String): Call<TwitterToken>
}