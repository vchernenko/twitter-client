package com.chernenkovit.feature_auth.domain.repository

import com.chernenkovit.base.data.model.TwitterToken
import com.chernenkovit.base.data.network.error.Failure
import com.chernenkovit.base.domain.functional.Either

interface AuthRepository {

    fun getGuestToken(): Either<Failure, TwitterToken>
}