package com.chernenkovit.feature_auth.domain.interactor

import com.chernenkovit.base.data.model.TwitterToken
import com.chernenkovit.base.data.network.error.Failure
import com.chernenkovit.base.domain.functional.Either
import com.chernenkovit.base.domain.interactor.UseCaseWithoutParams
import com.chernenkovit.feature_auth.domain.repository.AuthRepository
import javax.inject.Inject

class GetGuestTokenUseCase @Inject constructor(private val authRepository: AuthRepository) :
    UseCaseWithoutParams<TwitterToken>() {
    override suspend fun run(): Either<Failure, TwitterToken> {
        return authRepository.getGuestToken()
    }
}