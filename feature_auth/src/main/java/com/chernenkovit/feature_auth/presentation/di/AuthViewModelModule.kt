package com.chernenkovit.feature_auth.presentation.di


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chernenkovit.base.presentation.di.viewmodel.ViewModelFactory
import com.chernenkovit.base.presentation.di.viewmodel.ViewModelKey
import com.chernenkovit.feature_auth.presentation.ui.LoginViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class AuthViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindsLoginViewModel(loginViewModel: LoginViewModel): ViewModel
}