package com.chernenkovit.feature_auth.presentation.di

import com.chernenkovit.base.data.network.NetworkStateProvider
import com.chernenkovit.feature_auth.domain.repository.AuthRepository
import com.chernenkovit.feature_auth.presentation.ui.LoginViewModel
import retrofit2.Retrofit

interface AuthComponentApi {
    fun retrofit(): Retrofit
    fun networkStateProvider(): NetworkStateProvider
    fun authRepository(): AuthRepository
    fun loginViewModel(): LoginViewModel
}