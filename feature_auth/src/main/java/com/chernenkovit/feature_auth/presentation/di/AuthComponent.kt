package com.chernenkovit.feature_auth.presentation.di

import com.chernenkovit.base.presentation.di.BaseComponent
import com.chernenkovit.base.presentation.di.component.BaseAppComponentApi
import com.chernenkovit.base.presentation.di.scope.PerFeature
import com.chernenkovit.feature_auth.presentation.ui.LoginFragment
import dagger.Component
import java.lang.ref.WeakReference

@PerFeature
@Component(dependencies = [AuthDependencies::class], modules = [AuthDataModule::class, AuthViewModelModule::class])
interface AuthComponent : AuthComponentApi, BaseComponent {

    fun inject(fragment: LoginFragment)

    companion object {
        @Volatile
        private lateinit var authComponentWeak: WeakReference<AuthComponent>

        fun get(authDependencies: AuthDependencies): AuthComponent {
            if (!this::authComponentWeak.isInitialized || authComponentWeak.get() == null) {
                synchronized(AuthComponent::class) {
                    if (!this::authComponentWeak.isInitialized || authComponentWeak.get() == null) {
                        val component = DaggerAuthComponent.builder()
                            .authDependencies(authDependencies)
                            .build()
                        authComponentWeak = WeakReference(component)
                    }
                }
            }
            return authComponentWeak.get()!!
        }
    }

    @PerFeature
    @Component(dependencies = [BaseAppComponentApi::class])
    interface AuthDependenciesComponent : AuthDependencies
}