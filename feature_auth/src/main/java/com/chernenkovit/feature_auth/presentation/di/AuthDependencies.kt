package com.chernenkovit.feature_auth.presentation.di

import android.content.Context
import com.chernenkovit.base.data.network.NetworkStateProvider
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.storage.PreferenceUtils
import com.chernenkovit.base.presentation.navigator.Navigator
import retrofit2.Retrofit

interface AuthDependencies {
    fun retrofit(): Retrofit
    fun context(): Context
    fun networkStateProvider(): NetworkStateProvider
    fun preferenceUtils(): PreferenceUtils
    fun twitterAuth(): TwitterAuth
    fun navigator(): Navigator
}