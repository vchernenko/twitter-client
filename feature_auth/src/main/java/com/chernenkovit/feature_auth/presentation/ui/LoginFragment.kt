package com.chernenkovit.feature_auth.presentation.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chernenkovit.base.data.network.error.Failure
import com.chernenkovit.base.presentation.di.component.BaseAppComponent
import com.chernenkovit.base.presentation.extension.*
import com.chernenkovit.base.presentation.mvvm.BaseMvvmFragment
import com.chernenkovit.base.presentation.navigator.Navigator
import com.chernenkovit.feature_auth.R
import com.chernenkovit.feature_auth.presentation.di.AuthComponent
import com.chernenkovit.feature_auth.presentation.di.DaggerAuthComponent_AuthDependenciesComponent
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


class LoginFragment : BaseMvvmFragment<AuthComponent>(R.layout.fragment_login) {

    @Inject
    lateinit var navigator: Navigator
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initializeInjector()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel = viewModel(viewModelFactory) {
            observe(guestTokenLiveData, { renderGuestAuthSuccess() })
            failure(failure, ::handleFailure)
        }
        viewAuthGuestSession.setOnClickListener { authAsGuest() }
        viewAuthLogin.setOnClickListener { auth() }
        viewAuthRegister.setOnClickListener { register() }
    }

    private fun renderGuestAuthSuccess() {
        viewAuthProgress.invisible()
        navigator.goToTimeline(activity as Context)
    }

    private fun handleFailure(failure: Failure?) {
        viewAuthProgress.invisible()
        when (failure) {
            is Failure.NetworkConnection -> toast(getString(com.chernenkovit.base.R.string.failure_network_connection))
            is Failure.ServerError -> toast("${getString(com.chernenkovit.base.R.string.failure_server_error)} ${failure.code}")
            else -> toast(getString(com.chernenkovit.base.R.string.error_unknown))
        }
    }

    private fun authAsGuest() {
        viewAuthProgress.visible()
        loginViewModel.getGuestToken()
    }


    private fun auth() {
        toast("TBD")
    }

    private fun register() {
        toast("TBD")
    }

    override fun initializeInjector() {
        this.viewComponent = AuthComponent
            .get(
                DaggerAuthComponent_AuthDependenciesComponent.builder()
                    .baseAppComponentApi(BaseAppComponent.get())
                    .build()
            ).also { it.inject(this) }
    }
}
