package com.chernenkovit.feature_auth.presentation.di

import com.chernenkovit.base.presentation.di.scope.PerFeature
import com.chernenkovit.feature_auth.data.repository.AuthRepositoryImpl
import com.chernenkovit.feature_auth.domain.repository.AuthRepository
import dagger.Binds
import dagger.Module

@Module
abstract class AuthDataModule {
    @PerFeature
    @Binds
    abstract fun bindsAuthRepository(impl: AuthRepositoryImpl): AuthRepository
}