package com.chernenkovit.feature_auth.presentation.ui

import androidx.lifecycle.MutableLiveData
import com.chernenkovit.base.data.model.TwitterToken
import com.chernenkovit.base.presentation.mvvm.BaseViewModel
import com.chernenkovit.feature_auth.domain.interactor.GetGuestTokenUseCase
import javax.inject.Inject

class LoginViewModel
@Inject constructor(private val getGuestToken: GetGuestTokenUseCase) : BaseViewModel() {

    var guestTokenLiveData: MutableLiveData<TwitterToken> = MutableLiveData()

    fun getGuestToken() = getGuestToken { it.either(::handleFailure, ::handleGuestToken) }

    private fun handleGuestToken(guestToken: TwitterToken) {
        guestTokenLiveData.value = guestToken
    }
}