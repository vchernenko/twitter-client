Twitter Client
===================

Twitter Client is assessment task to show skills of Android development and architecture.


Project is based on multi modules architecture (divided by features) with Instant App implementation.  
**Auth feature** is based on Clean Architecture and MVVM pattern (using coroutines).  
**Timeline feature** is based on Clean Architecture and MVP pattern (using Rx).  

Features to be implemented in future
-----------------------------------
* Implement Navigation with states handling
* Implement feaure "Favourite" with offline caching based on Room
* Write end-to-end (Robolectric), unit and some UI tests

Features and libraries
-----------------------------------
* [AndroidX libraries](https://developer.android.com/jetpack/androidx)
* [Dagger2](https://github.com/google/dagger)
* [Retrofit2](https://square.github.io/retrofit/)
* [RxJava](https://github.com/ReactiveX/RxJava) / [RxAndroid](https://github.com/ReactiveX/RxAndroid) / [RxBinding](https://github.com/JakeWharton/RxBinding)
* [Mosby](https://github.com/sockeqwe/mosby)
* [Glide](https://github.com/bumptech/glide)
* [Epoxy](https://github.com/airbnb/epoxy)

Screenshots
-----------
<img src="design/screenshot-05.png" alt="Initial screen" width="250"/>   <img src="design/screenshot-06.png" alt="Initial screen" width="250"/>

<img src="design/screenshot-01.png" alt="Initial screen" width="250"/>   <img src="design/screenshot-02.png" alt="Initial screen" width="250"/>

<img src="design/screenshot-03.png" alt="Initial screen" width="250"/>   <img src="design/screenshot-04.png" alt="Initial screen" width="250"/>

-------------------------------------------------------------------------------

Developed By
============

* Vitalii Chernenko - <chernenkovit@gmail.com>

License
=======
```
The MIT License (MIT)

Copyright (c) 2019 Vitalii Chernenko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
