package com.chernenkovit.timeline.di.activity

import com.chernenkovit.base.presentation.di.scope.PerActivity
import com.chernenkovit.timeline.MainActivity
import com.chernenkovit.timeline.di.activity.module.ActivityContextModule
import com.chernenkovit.timeline.di.feature.DashboardComponentApi
import dagger.Component

@PerActivity
@Component(
    dependencies = [DashboardActivityDependencies::class],
    modules = [ActivityContextModule::class]
)
interface DashboardActivityComponent : DashboardActivityComponentApi {

    fun inject(activity: MainActivity)

    @Component(dependencies = [DashboardComponentApi::class])
    @PerActivity
    interface DashboardActivityDependenciesComponent : DashboardActivityDependencies
}