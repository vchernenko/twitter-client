package com.chernenkovit.timeline.di.activity.module

import android.app.Activity
import android.content.Context
import com.chernenkovit.base.presentation.ResourcesProvider
import com.chernenkovit.base.presentation.di.scope.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityContextModule(private val activity: Activity) {
    @Provides
    @PerActivity
    fun provideActivity(): Context {
        return activity
    }

    @Provides
    @PerActivity
    fun provideResources(context: Context): ResourcesProvider {
        return ResourcesProvider(context)
    }

}