package com.chernenkovit.timeline.di.feature

import com.chernenkovit.base.presentation.di.scope.PerFeature
import com.chernenkovit.timeline.timeline.data.repository.TimelineRepositoryImpl
import com.chernenkovit.timeline.timeline.domain.repository.TimelineRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DashboardDataModule {
    @Binds
    @PerFeature
    abstract fun bindRepository(impl: TimelineRepositoryImpl): TimelineRepository
}