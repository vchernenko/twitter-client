package com.chernenkovit.timeline.di.feature

import com.chernenkovit.base.presentation.di.component.BaseAppComponentApi
import com.chernenkovit.base.presentation.di.scope.PerFeature
import dagger.Component
import java.lang.ref.WeakReference

@PerFeature
@Component(dependencies = [DashboardDependencies::class], modules = [DashboardDataModule::class])
interface DashboardComponent : DashboardComponentApi {

  companion object {
    @Volatile
    private lateinit var dashComponentWeak: WeakReference<DashboardComponent>
    fun get(dashDependencies: DashboardDependencies): DashboardComponent {
      if (!this::dashComponentWeak.isInitialized || dashComponentWeak.get() == null) {
        synchronized(DashboardComponent::class) {
          if (!this::dashComponentWeak.isInitialized || dashComponentWeak.get() == null) {
            val component = DaggerDashboardComponent.builder()
                .dashboardDependencies(dashDependencies)
                .build()
            dashComponentWeak = WeakReference(component)
          }
        }
      }
      return dashComponentWeak.get()!!
    }
  }

  @PerFeature
  @Component(dependencies = [BaseAppComponentApi::class])
  interface DashboardDependenciesComponent : DashboardDependencies
}