package com.chernenkovit.timeline.di.feature

import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.data.network.NetworkStateProvider
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.storage.PreferenceUtils
import com.chernenkovit.base.data.storage.TwitterTokenProvider
import com.chernenkovit.timeline.timeline.domain.repository.TimelineRepository
import retrofit2.Retrofit

interface DashboardComponentApi {
    fun timelineRepository(): TimelineRepository
    fun retrofit(): Retrofit
    fun networkHandler(): NetworkStateProvider
    fun preferenceUtils(): PreferenceUtils
    fun twitterAuth(): TwitterAuth
    fun twitterTokenProvider(): TwitterTokenProvider
    fun threadExecutor(): ThreadExecutor
    fun postExecutionThread(): PostExecutionThread
//    fun resourcesProvider(): ResourcesProvider
}