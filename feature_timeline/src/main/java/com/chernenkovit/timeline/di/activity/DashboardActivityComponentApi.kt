package com.chernenkovit.timeline.di.activity

import android.content.Context
import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.storage.TwitterTokenProvider
import com.chernenkovit.base.presentation.ResourcesProvider
import com.chernenkovit.timeline.timeline.domain.repository.TimelineRepository

interface DashboardActivityComponentApi {
    fun context(): Context
    fun timelineRepository(): TimelineRepository
    fun twitterAuth(): TwitterAuth
    fun twitterTokenProvider(): TwitterTokenProvider
    fun threadExecutor(): ThreadExecutor
    fun postExecutionThread(): PostExecutionThread
    fun resourcesProvider(): ResourcesProvider

}