package com.chernenkovit.timeline.di.feature

import android.content.Context
import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.storage.PreferenceUtils
import com.chernenkovit.base.data.storage.TwitterTokenProvider
import com.chernenkovit.base.data.network.NetworkStateProvider
import retrofit2.Retrofit

interface DashboardDependencies {
  fun retrofit(): Retrofit
  fun context(): Context
  fun networkHandler(): NetworkStateProvider
  fun preferenceUtils(): PreferenceUtils
  fun twitterAuth(): TwitterAuth
  fun twitterTokenProvider(): TwitterTokenProvider
  fun threadExecutor(): ThreadExecutor
  fun postExecutionThread(): PostExecutionThread
//  fun resourcesProvider(): ResourcesProvider
}