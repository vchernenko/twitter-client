package com.chernenkovit.timeline

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.chernenkovit.base.presentation.di.HasComponent
import com.chernenkovit.base.presentation.di.component.BaseAppComponent
import com.chernenkovit.base.presentation.extension.bindView
import com.chernenkovit.timeline.di.activity.DaggerDashboardActivityComponent
import com.chernenkovit.timeline.di.activity.DaggerDashboardActivityComponent_DashboardActivityDependenciesComponent
import com.chernenkovit.timeline.di.activity.DashboardActivityComponent
import com.chernenkovit.timeline.di.activity.module.ActivityContextModule
import com.chernenkovit.timeline.di.feature.DaggerDashboardComponent_DashboardDependenciesComponent
import com.chernenkovit.timeline.di.feature.DashboardComponent
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), HasComponent<DashboardActivityComponent> {

    private lateinit var dashboardActivityComponent: DashboardActivityComponent
    lateinit var navController: NavController
    private val bottomBar: BottomNavigationView by bindView(R.id.bottomNavigationView)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeInjector()
        setContentView(R.layout.activity_main)
        getComponent().inject(this)
        navController = Navigation.findNavController(this, R.id.navHostFragment)
        navController.setGraph(R.navigation.timeline_graph)
        NavigationUI.setupWithNavController(bottomBar, navController)
    }

    override fun initializeInjector() {
        val dashboardActivityDependencies = DaggerDashboardActivityComponent_DashboardActivityDependenciesComponent
            .builder()
            .dashboardComponentApi(
                DashboardComponent
                    .get(
                        DaggerDashboardComponent_DashboardDependenciesComponent
                            .builder()
                            .baseAppComponentApi((application as HasComponent<*>).getComponent() as BaseAppComponent)
                            .build()
                    )
            )
            .build()
        dashboardActivityComponent = DaggerDashboardActivityComponent
            .builder()
            .dashboardActivityDependencies(dashboardActivityDependencies)
            .activityContextModule(ActivityContextModule(this))
            .build()
    }

    override fun getComponent(): DashboardActivityComponent = dashboardActivityComponent
}
