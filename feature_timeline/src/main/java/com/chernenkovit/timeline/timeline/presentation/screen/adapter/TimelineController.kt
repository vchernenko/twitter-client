package com.chernenkovit.timeline.timeline.presentation.screen.adapter

import android.net.Uri
import com.airbnb.epoxy.AutoModel
import com.airbnb.epoxy.Typed3EpoxyController
import com.chernenkovit.timeline.timeline.presentation.model.TimelineItemModel
import com.chernenkovit.timeline.timeline.presentation.screen.adapter.holder.*
import javax.inject.Inject

class TimelineController @Inject constructor() : Typed3EpoxyController<List<TimelineItemModel>, Boolean, Boolean>() {

    @AutoModel
    lateinit var initialProgresss: ProgressFullViewModel_
    @AutoModel
    lateinit var emptyView: EmptyViewModel_
    @AutoModel
    lateinit var progressBar: ProgressViewModel_
    private var timelineClickListener: TimelineClickListener? = null
    private var itemsList = mutableListOf<TimelineItemModel>()

    init {
        setFilterDuplicates(true)
    }

    override fun buildModels(items: List<TimelineItemModel>, hasMore: Boolean, showInitialProgress: Boolean) {
        itemsList.addAll(items)
        addInitialProgress(items.isNullOrEmpty() && showInitialProgress)
        items.forEach {
            addTweet(it)
            addSeparator(it)
        }

        progressBar.addIf(hasMore, this)
        emptyView.addIf(itemsList.isNullOrEmpty() && !showInitialProgress, this)
    }

    private fun addInitialProgress(isListEmpty: Boolean) {
        initialProgresss.addIf(
            isListEmpty,
            this
        )
    }


    private fun addTweet(tweet: TimelineItemModel) {
        TimelineViewModel_()
            .id(tweet.id)
            .photoUri(Uri.parse(tweet.author.avatarUrl))
            .name(tweet.author.name)
            .handle(tweet.author.handle)
            .date(tweet.date)
            .message(tweet.message)
            .onItemClickListener { _, _, _, _ -> timelineClickListener?.onItemClick(tweet) }
            .addTo(this)
    }

    private fun addSeparator(tweet: TimelineItemModel) {
        SeparatorFullWidthModel_()
            .id(tweet.id.toString())
            .addTo(this)
    }
}

interface TimelineClickListener {
    fun onItemClick(tweet: TimelineItemModel)
}
