package com.chernenkovit.timeline.timeline.data.model

import com.google.gson.annotations.SerializedName
import java.math.BigInteger
import java.util.*

/** Twitter timeline entity.  */
class TimelineItemEntity(
    /** Timeline ID.  */
    @SerializedName("id") val id: BigInteger = BigInteger.ZERO,
    /** User.  */
    @SerializedName("user") val author: TwitterUserEntity,
    /** Timeline message.  */
    @SerializedName("text") val message: String = "",
    /** Timeline date.  */
    @SerializedName("created_at") val date: Date
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TimelineItemEntity

        if (id != other.id) return false
        if (author != other.author) return false
        if (message != other.message) return false
        if (date != other.date) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + author.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + message.hashCode()
        result = 31 * result + date.hashCode()
        return result
    }
}