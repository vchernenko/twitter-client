package com.chernenkovit.timeline.timeline.data.mapper

import com.chernenkovit.base.domain.mapper.Mapper
import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import com.chernenkovit.timeline.timeline.domain.model.TimelineItem
import javax.inject.Inject

class TimelineItemEntityMapper @Inject constructor(val twitterUserEntityMapper: TwitterUserEntityMapper) :
    Mapper<TimelineItemEntity, TimelineItem>() {
    override fun reverse(to: TimelineItem): TimelineItemEntity = to.run {
        TimelineItemEntity(
            id, twitterUserEntityMapper.reverse(author), message, date
        )
    }

    override fun map(from: TimelineItemEntity): TimelineItem = from.run {
        TimelineItem(id, twitterUserEntityMapper.map(author), message, date)
    }
}