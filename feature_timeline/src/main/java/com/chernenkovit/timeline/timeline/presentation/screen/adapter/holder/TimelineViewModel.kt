package com.chernenkovit.timeline.timeline.presentation.screen.adapter.holder

import android.net.Uri
import android.text.format.DateUtils
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyAttribute.Option.DoNotHash
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.Glide
import com.chernenkovit.base.presentation.adapter.holder.BaseHolder
import com.chernenkovit.base.presentation.utils.ImageUtils
import com.chernenkovit.timeline.R
import java.util.*

@EpoxyModelClass
abstract class TimelineViewModel : EpoxyModelWithHolder<TimelineHolder>() {
    @EpoxyAttribute
    lateinit var photoUri: Uri
    @EpoxyAttribute
    lateinit var name: String
    @EpoxyAttribute
    lateinit var handle: String
    @EpoxyAttribute
    lateinit var date: Date
    @EpoxyAttribute
    lateinit var message: String
    @EpoxyAttribute(DoNotHash)
    lateinit var onItemClickListener: View.OnClickListener

    override fun getDefaultLayout(): Int = R.layout.view_timeline_model

    override fun bind(holder: TimelineHolder) {
        with(holder) {
            val now = System.currentTimeMillis()
            val time = DateUtils.getRelativeTimeSpanString(
                date.time, now, DateUtils.HOUR_IN_MILLIS,
                DateUtils.FORMAT_NUMERIC_DATE
            )

            ImageUtils.loadPhoto(imageView, photoUri)

            nameView.text = name
            if (handle.startsWith("@")) {
                handleView.text = handle
            } else handleView.text = "@$handle"
            timeView.text = time
            messageView.text = message

            imageView.setOnClickListener(onItemClickListener)
        }
    }

    override fun unbind(holder: TimelineHolder) {
        with(holder) {
            holder.imageView.setOnClickListener(null)
            Glide.with(imageView).clear(imageView)
        }
    }
}

class TimelineHolder : BaseHolder() {
    val imageView by bind<ImageView>(R.id.timelineItemViewIcon)
    val nameView by bind<AppCompatTextView>(R.id.timelineItemViewName)
    val handleView by bind<AppCompatTextView>(R.id.timelineItemViewHandle)
    val timeView by bind<AppCompatTextView>(R.id.timelineItemViewTime)
    val messageView by bind<AppCompatTextView>(R.id.timelineItemViewMessage)
}