package com.chernenkovit.timeline.timeline.domain.repository

import com.chernenkovit.timeline.timeline.domain.model.TimelineItem
import io.reactivex.Single
import java.math.BigInteger

interface TimelineRepository {

    fun getUserTimeline(
        username: String,
        count: Int,
        maxId: BigInteger?
    ): Single<List<TimelineItem>>
}