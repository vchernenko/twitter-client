package com.chernenkovit.timeline.timeline.presentation.contract

import com.chernenkovit.timeline.timeline.presentation.model.TimelineItemModel
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface TimelineContract {
    interface Presenter : MvpPresenter<View>

    interface View : MvpView {
        fun showData(list: List<TimelineItemModel>, hasMore: Boolean, showProgress: Boolean)

        fun showError(message: String)

        fun getLoadObservable(): Observable<CharSequence>

    }

}