package com.chernenkovit.timeline.timeline.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TwitterUserModel(
    val id: Long = 0L,
    val name: String = "",
    val handle: String = "",
    val avatarUrl: String = ""
) : Parcelable
