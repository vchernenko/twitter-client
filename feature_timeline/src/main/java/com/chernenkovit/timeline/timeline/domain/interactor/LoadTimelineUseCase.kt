package com.chernenkovit.timeline.timeline.domain.interactor

import com.chernenkovit.base.data.executor.PostExecutionThread
import com.chernenkovit.base.data.executor.ThreadExecutor
import com.chernenkovit.base.domain.interactor.SingleUseCase
import com.chernenkovit.timeline.timeline.domain.mapper.TimelineItemMapper
import com.chernenkovit.timeline.timeline.domain.repository.TimelineRepository
import com.chernenkovit.timeline.timeline.presentation.model.TimelineItemModel
import io.reactivex.Single
import java.math.BigInteger
import javax.inject.Inject

class LoadTimelineUseCase @Inject constructor(
    private val timelineRepository: TimelineRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    private val timelineItemMapper: TimelineItemMapper
) : SingleUseCase<List<TimelineItemModel>, LoadTimelineUseCase.Params>(
    threadExecutor, postExecutionThread
) {

    override fun buildUseCaseObservable(params: LoadTimelineUseCase.Params): Single<List<TimelineItemModel>> =
        timelineRepository.getUserTimeline(params.userName, params.count, params.maxId).map { timelineItemMapper.map(it) }

    class Params(val userName: String, val count: Int, val maxId: BigInteger?)

}
