package com.chernenkovit.timeline.timeline.data.model

import com.google.gson.annotations.SerializedName


/** TwitterToken response.  */
class TwitterToken {
    /** TwitterToken type.  */
    @SerializedName("token_type")
    var type: String = ""
    /** Access token.  */
    @SerializedName("access_token")
    var token: String = ""

    override fun toString(): String {
        return "$type $token"
    }
}
