package com.chernenkovit.timeline.timeline.domain.mapper

import com.chernenkovit.base.domain.mapper.Mapper
import com.chernenkovit.timeline.timeline.domain.model.TimelineItem
import com.chernenkovit.timeline.timeline.presentation.model.TimelineItemModel
import javax.inject.Inject

class TimelineItemMapper @Inject constructor(private val twitterUserMapper: TwitterUserMapper) :
    Mapper<TimelineItem, TimelineItemModel>() {
    override fun reverse(to: TimelineItemModel): TimelineItem = to.run {
        TimelineItem(id, twitterUserMapper.reverse(author), message, date)
    }

    override fun map(from: TimelineItem): TimelineItemModel = from.run {
        TimelineItemModel(id, twitterUserMapper.map(author), message, date)
    }
}