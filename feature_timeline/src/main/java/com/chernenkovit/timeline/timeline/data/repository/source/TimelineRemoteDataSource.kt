package com.chernenkovit.timeline.timeline.data.repository.source

import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import com.chernenkovit.timeline.timeline.data.net.TimelineApiImpl
import io.reactivex.Single
import java.math.BigInteger
import javax.inject.Inject

class TimelineRemoteDataSource @Inject constructor(
    private val timelineApiImpl: TimelineApiImpl
) : TimelineDataSource {

    override fun getUserTimeline(
        username: String,
        count: Int,
        maxId: BigInteger?
    ): Single<List<TimelineItemEntity>> = timelineApiImpl.getUserTimeline(username, count, maxId)
}