package com.chernenkovit.timeline.timeline.domain.model

class TwitterUser(
    val id: Long = 0L,
    val name: String = "",
    val handle: String = "",
    val avatarUrl: String = ""
)
