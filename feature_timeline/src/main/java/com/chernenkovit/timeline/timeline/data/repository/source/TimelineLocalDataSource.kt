package com.chernenkovit.timeline.timeline.data.repository.source

import com.chernenkovit.base.data.model.error.ApiErrorException
import com.chernenkovit.base.data.model.error.ApiErrorException.Companion.ERROR_NO_INTERNET
import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import io.reactivex.Single
import java.math.BigInteger
import javax.inject.Inject

class TimelineLocalDataSource @Inject constructor() : TimelineDataSource {

    override fun getUserTimeline(username: String, count: Int, maxId: BigInteger?): Single<List<TimelineItemEntity>> {
        return Single.error(ApiErrorException(ERROR_NO_INTERNET))
    }
}