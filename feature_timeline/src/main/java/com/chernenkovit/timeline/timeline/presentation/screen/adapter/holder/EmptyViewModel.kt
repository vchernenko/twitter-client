package com.chernenkovit.timeline.timeline.presentation.screen.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.base.presentation.adapter.holder.BaseHolder
import com.chernenkovit.timeline.R

@EpoxyModelClass
abstract class EmptyViewModel : EpoxyModelWithHolder<EmptyViewModelHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_empty_list_model

    override fun bind(holder: EmptyViewModelHolder) {
    }
}

//add and customize attributes if necessary
class EmptyViewModelHolder : BaseHolder() {
}

