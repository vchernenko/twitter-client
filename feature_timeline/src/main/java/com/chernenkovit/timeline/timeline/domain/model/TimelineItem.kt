package com.chernenkovit.timeline.timeline.domain.model

import java.math.BigInteger
import java.util.*

class TimelineItem(
    val id: BigInteger = BigInteger.ZERO,
    val author: TwitterUser,
    val message: String = "",
    val date: Date
)