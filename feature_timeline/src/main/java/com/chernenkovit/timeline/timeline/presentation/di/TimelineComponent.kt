package com.chernenkovit.timeline.timeline.presentation.di

import com.chernenkovit.base.presentation.di.BaseComponent
import com.chernenkovit.base.presentation.di.scope.PerScreen
import com.chernenkovit.timeline.di.activity.DashboardActivityComponent
import com.chernenkovit.timeline.timeline.presentation.screen.TimelineFragment
import dagger.Component

@PerScreen
@Component(
    dependencies = [DashboardActivityComponent::class],
    modules = [TimelineModule::class]
)
interface TimelineComponent : BaseComponent {
    fun inject(fragment: TimelineFragment)
}