package com.chernenkovit.timeline.timeline.presentation.presenter

import com.chernenkovit.base.presentation.ResourcesProvider
import com.chernenkovit.base.presentation.mvp.BasePresenter
import com.chernenkovit.base.presentation.utils.DevUtils
import com.chernenkovit.timeline.timeline.domain.interactor.LoadTimelineUseCase
import com.chernenkovit.timeline.timeline.presentation.contract.TimelineContract
import com.chernenkovit.timeline.timeline.presentation.model.TimelineItemModel
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TimelinePresenterImpl @Inject constructor(
    private val loadTimelineUseCase: LoadTimelineUseCase,
    private val resourcesProvider: ResourcesProvider
) :
    BasePresenter<TimelineContract.View>(), TimelineContract.Presenter {

    private var tweetList: MutableList<TimelineItemModel> = mutableListOf()
    private val TIMELINE_PAGE_SIZE = 30
    private val DEBOUNCE_TIMEOUT = 1500L
    private var username: String? = null
    private var hasMore: Boolean = false


    override fun attachView(view: TimelineContract.View) {
        super.attachView(view)
        if (tweetList.isNotEmpty()) {
            view.showData(tweetList, hasMore, false)
        }
        subscriptions?.add(view.getLoadObservable()
            .filter { user -> user.isNotEmpty() }
            // prevent to often calls
            .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .map { charSequence -> charSequence.toString() }
            .doOnNext { username -> setUsername(username) }
            .subscribe { username -> loadTimeline(username) })
    }

    private fun setUsername(user: String) {
        // reset user and clear timeline history
        if (user != username) {
            tweetList.clear()
            username = user
        }
    }

    private fun loadTimeline(username: String) {
        val params = LoadTimelineUseCase.Params(username, TIMELINE_PAGE_SIZE, tweetList.lastOrNull()?.id)
        subscriptions?.add(loadTimelineUseCase.executeWithoutSubscription(params)
            .doOnSubscribe {
                ifViewAttached { view ->
                    if (tweetList.isNullOrEmpty()) {
                        view.showData(tweetList, hasMore = false, showProgress = true)
                    }
                }
            }
            .subscribe({ list ->
                tweetList.addAll(list)
                ifViewAttached { view ->
                    hasMore = list.size + 1 >= TIMELINE_PAGE_SIZE
                    list.let {
                        view.showData(tweetList, hasMore, false)
                    }
                }
            },
                { throwable ->
                    ifViewAttached { view ->
                        view.showData(emptyList(), hasMore = false, showProgress = false)
                        view.showError(throwable.message ?: resourcesProvider.getString(com.chernenkovit.base.R.string.error_unknown))
                    }
                    DevUtils.log(throwable)
                })
        )
    }
}