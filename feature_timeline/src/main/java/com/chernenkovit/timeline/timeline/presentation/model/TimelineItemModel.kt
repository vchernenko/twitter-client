package com.chernenkovit.timeline.timeline.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigInteger
import java.util.*

@Parcelize
data class TimelineItemModel(
    val id: BigInteger = BigInteger.ZERO,
    val author: TwitterUserModel,
    val message: String = "",
    val date: Date
) : Parcelable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TimelineItemModel

        if (id != other.id) return false
        if (author != other.author) return false
        if (message != other.message) return false
        if (date != other.date) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + author.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + message.hashCode()
        result = 31 * result + date.hashCode()
        return result
    }
}