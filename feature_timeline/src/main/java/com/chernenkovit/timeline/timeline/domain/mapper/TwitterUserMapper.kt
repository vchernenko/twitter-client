package com.chernenkovit.timeline.timeline.domain.mapper

import com.chernenkovit.base.domain.mapper.Mapper
import com.chernenkovit.timeline.timeline.domain.model.TwitterUser
import com.chernenkovit.timeline.timeline.presentation.model.TwitterUserModel
import javax.inject.Inject

class TwitterUserMapper @Inject constructor() : Mapper<TwitterUser, TwitterUserModel>() {
    override fun reverse(to: TwitterUserModel): TwitterUser = to.run {
        TwitterUser(id, name, handle, avatarUrl)
    }

    override fun map(from: TwitterUser): TwitterUserModel = from.run {
        TwitterUserModel(id, name, handle, avatarUrl)
    }
}