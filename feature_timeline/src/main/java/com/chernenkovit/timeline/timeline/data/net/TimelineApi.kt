package com.chernenkovit.timeline.timeline.data.net

import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import io.reactivex.Single
import java.math.BigInteger

interface TimelineApi {

    fun getUserTimeline(
        username: String,
        count: Int,
        maxId: BigInteger?
    ): Single<List<TimelineItemEntity>>
}