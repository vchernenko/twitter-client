package com.chernenkovit.timeline.timeline.data.repository.source

import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import io.reactivex.Single
import java.math.BigInteger

interface TimelineDataSource {

    fun getUserTimeline(
        username: String,
        count: Int,
        maxId: BigInteger?
    ): Single<List<TimelineItemEntity>>
}