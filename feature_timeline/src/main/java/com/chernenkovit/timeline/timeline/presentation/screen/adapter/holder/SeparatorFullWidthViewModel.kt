package com.chernenkovit.timeline.timeline.presentation.screen.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.base.presentation.adapter.holder.BaseHolder
import com.chernenkovit.timeline.R


@EpoxyModelClass
abstract class SeparatorFullWidthModel : EpoxyModelWithHolder<SeparatorHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_separator_full_width

    override fun bind(holder: SeparatorHolder) {
    }
}

//add and customize attributes if necessary
class SeparatorHolder : BaseHolder() {
}

