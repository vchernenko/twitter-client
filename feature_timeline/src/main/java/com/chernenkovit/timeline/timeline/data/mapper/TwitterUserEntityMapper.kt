package com.chernenkovit.timeline.timeline.data.mapper

import com.chernenkovit.base.domain.mapper.Mapper
import com.chernenkovit.timeline.timeline.data.model.TwitterUserEntity
import com.chernenkovit.timeline.timeline.domain.model.TwitterUser
import javax.inject.Inject

class TwitterUserEntityMapper @Inject constructor() : Mapper<TwitterUserEntity, TwitterUser>() {
    override fun reverse(to: TwitterUser): TwitterUserEntity = to.run {
        TwitterUserEntity(id, name, handle, avatarUrl)
    }

    override fun map(from: TwitterUserEntity): TwitterUser = from.run {
        TwitterUser(id, name, handle, avatarUrl)
    }
}