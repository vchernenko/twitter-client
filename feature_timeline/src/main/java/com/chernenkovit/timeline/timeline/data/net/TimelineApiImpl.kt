package com.chernenkovit.timeline.timeline.data.net

import com.chernenkovit.base.data.model.TwitterToken
import com.chernenkovit.base.data.model.asOptional
import com.chernenkovit.base.data.network.TwitterAuth
import com.chernenkovit.base.data.storage.TwitterTokenProvider
import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import io.reactivex.Single
import retrofit2.Retrofit
import java.math.BigInteger
import javax.inject.Inject

class TimelineApiImpl @Inject constructor(
    retrofit: Retrofit,
    private val tokenProvider: TwitterTokenProvider,
    private val twitterAuth: TwitterAuth
) : TimelineApi {

    private val timelineApi by lazy { retrofit.create(TimelineApiService::class.java) }

    override fun getUserTimeline(
        username: String,
        count: Int,
        maxId: BigInteger?
    ): Single<List<TimelineItemEntity>> =
        getToken(twitterAuth.toString()).flatMap { token ->
            timelineApi.getUserTimeline(
                username,
                count,
                maxId
            )
        }

    private fun getToken(auth: String): Single<TwitterToken> {
        return Single.just(tokenProvider.twitterToken.asOptional())
            // check whether token has already been requested
            .flatMap { optionalToken ->
                if (optionalToken.value == null) {
                    // request token and store it
                    return@flatMap timelineApi.getToken(auth)
                        .doOnSuccess { token -> tokenProvider.twitterToken = token }
                }
                Single.just(tokenProvider.twitterToken)
            }
    }

}