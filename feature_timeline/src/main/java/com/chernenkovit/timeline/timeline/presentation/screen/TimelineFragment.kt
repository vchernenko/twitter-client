package com.chernenkovit.timeline.timeline.presentation.screen

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chernenkovit.base.presentation.di.HasComponent
import com.chernenkovit.base.presentation.mvp.BaseFragment
import com.chernenkovit.base.presentation.utils.ImageUtils
import com.chernenkovit.timeline.di.activity.DashboardActivityComponent
import com.chernenkovit.timeline.timeline.presentation.contract.TimelineContract
import com.chernenkovit.timeline.timeline.presentation.di.DaggerTimelineComponent
import com.chernenkovit.timeline.timeline.presentation.di.TimelineComponent
import com.chernenkovit.timeline.timeline.presentation.model.TimelineItemModel
import com.chernenkovit.timeline.timeline.presentation.model.TwitterUserModel
import com.chernenkovit.timeline.timeline.presentation.screen.adapter.TimelineController
import com.jakewharton.rxbinding3.recyclerview.scrollStateChanges
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import kotlinx.android.synthetic.main.dashboard_toolbar.*
import kotlinx.android.synthetic.main.fragment_timeline.*
import javax.inject.Inject

class TimelineFragment :
    BaseFragment<TimelineContract.View, TimelineContract.Presenter, TimelineComponent>(
        com.chernenkovit.timeline.R.layout.fragment_timeline
    ), TimelineContract.View {

    @Inject
    lateinit var timelineController: TimelineController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewTimelineItemsList.adapter != null) return
        viewTimelineItemsList.layoutManager = LinearLayoutManager(context)
        viewTimelineItemsList.adapter = timelineController.adapter
        viewToolbarSearch.setOnClickListener { viewTimelineUsername.requestFocus() }
        viewTimelineUsername.requestFocus()
    }

    override fun getLoadObservable(): Observable<CharSequence> {
        val scrolling: Observable<CharSequence> = viewTimelineItemsList.scrollStateChanges()
            .filter { state -> state == RecyclerView.SCROLL_STATE_IDLE }
            .map { state -> viewTimelineItemsList.layoutManager as LinearLayoutManager }
            .filter { layoutManager ->
                // check whether last item is visible
                val itemCount = layoutManager.itemCount
                val visibleItems = layoutManager.childCount
                val firstVisiblePosition = layoutManager.findFirstVisibleItemPosition()
                visibleItems + firstVisiblePosition >= itemCount && firstVisiblePosition > 0
            }
            .map { layoutManager -> viewTimelineUsername.text }
        val textChange: Observable<CharSequence> = viewTimelineUsername.textChanges()
        return Observable.merge(textChange, scrolling)
    }

    override fun showData(list: List<TimelineItemModel>, hasMore: Boolean, showProgress: Boolean) {
        timelineController.setData(list, hasMore, showProgress)
        if (list.isNotEmpty()) showAuthor(list[0].author)
    }

    private fun showAuthor(author: TwitterUserModel) {
        ImageUtils.loadPhoto(viewToolbarUserAvatar, author.avatarUrl)
        viewToolbarUserName.text = author.name
    }

    override fun showError(message: String) {
        ImageUtils.loadPhoto(viewToolbarUserAvatar, com.chernenkovit.base.R.drawable.ic_man)
        viewToolbarUserName.text = ""
        val toast = Toast.makeText(activity, message, LENGTH_LONG)
        val yOffset = Math.max(
            0,
            timelineRoot.height - resources.getDimensionPixelSize(com.chernenkovit.base.R.dimen.toast_bottom_margin)/*toast.yOffset*/
        )
        toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, yOffset)
        toast.show()
    }

    override fun initializeInjector() {
        this.viewComponent = DaggerTimelineComponent.builder()
            .dashboardActivityComponent(
                (activity as HasComponent<*>).getComponent() as DashboardActivityComponent
            )
            .build()
        viewComponent.inject(this)
    }
}
