package com.chernenkovit.timeline.timeline.data.repository

import com.chernenkovit.timeline.timeline.data.mapper.TimelineItemEntityMapper
import com.chernenkovit.timeline.timeline.data.repository.source.TimelineDataSourceProvider
import com.chernenkovit.timeline.timeline.domain.model.TimelineItem
import com.chernenkovit.timeline.timeline.domain.repository.TimelineRepository
import io.reactivex.Single
import java.math.BigInteger
import javax.inject.Inject

class TimelineRepositoryImpl @Inject constructor(
    private val dataSourceProvider: TimelineDataSourceProvider,
    private val timelineItemEntityMapper: TimelineItemEntityMapper
) : TimelineRepository {

    override fun getUserTimeline(
        username: String,
        count: Int,
        maxId: BigInteger?
    ): Single<List<TimelineItem>> =
        dataSourceProvider.retrieveDataStore().getUserTimeline(username, count, maxId).map {
            timelineItemEntityMapper.map(
                it
            )
        }
}