package com.chernenkovit.timeline.timeline.data.repository.source

import com.chernenkovit.base.data.network.NetworkStateProvider
import dagger.Lazy
import javax.inject.Inject

class TimelineDataSourceProvider @Inject constructor(
    private val remoteDataSource: Lazy<TimelineRemoteDataSource>,
    private val localDataSource: Lazy<TimelineLocalDataSource>,
    private val networkStateProvider: NetworkStateProvider

) {
    fun retrieveDataStore(): TimelineDataSource =
        if (networkStateProvider.isOnline) retrieveRemoteDataStore() else retrieveLocalDataStore()

    private fun retrieveRemoteDataStore(): TimelineRemoteDataSource = remoteDataSource.get()

    private fun retrieveLocalDataStore(): TimelineLocalDataSource = localDataSource.get()
}