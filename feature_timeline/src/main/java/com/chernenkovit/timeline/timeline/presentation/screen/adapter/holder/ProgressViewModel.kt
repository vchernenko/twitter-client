package com.chernenkovit.timeline.timeline.presentation.screen.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.base.presentation.adapter.holder.BaseHolder
import com.chernenkovit.timeline.R

@EpoxyModelClass
abstract class ProgressViewModel : EpoxyModelWithHolder<ProgressHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_progress_bar_model

    override fun bind(holder: ProgressHolder) {
    }
}

//add and customize attributes if necessary
class ProgressHolder : BaseHolder() {
}





