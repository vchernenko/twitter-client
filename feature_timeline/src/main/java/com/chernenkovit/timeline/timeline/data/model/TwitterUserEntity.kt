package com.chernenkovit.timeline.timeline.data.model

import com.google.gson.annotations.SerializedName

/** Twitter user.  */
class TwitterUserEntity(
    /** User name.  */
    @SerializedName("id")
    val id: Long = 0L,
    /** User name.  */
    @SerializedName("name")
    var name: String = "",
    /** User handle.  */
    @SerializedName("screen_name")
    var handle: String = "",
    /*  get() = if (handle.startsWith("@")) {
          field
      } else "@$field"*/
    /** User avatar URL.  */
    @SerializedName("profile_image_url_https")
    val avatarUrl: String = ""
)

/* fun getHandle(): String {
     return if (handle.startsWith("@")) {
         handle
     } else "@$handle"
 }*/

