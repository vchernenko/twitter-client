package com.chernenkovit.timeline.timeline.presentation.di

import com.chernenkovit.base.presentation.di.scope.PerScreen
import com.chernenkovit.timeline.timeline.presentation.contract.TimelineContract
import com.chernenkovit.timeline.timeline.presentation.presenter.TimelinePresenterImpl
import dagger.Binds
import dagger.Module

@Module
abstract class TimelineModule {

    @PerScreen
    @Binds
    abstract fun bindTimelinePresenter(presenter: TimelinePresenterImpl): TimelineContract.Presenter
}