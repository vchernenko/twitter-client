package com.chernenkovit.timeline.timeline.data.net

import com.chernenkovit.base.data.model.TwitterToken
import com.chernenkovit.timeline.timeline.data.model.TimelineItemEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query
import java.math.BigInteger

const val AUTH_ENDPOINT = "oauth2/token?grant_type=client_credentials"
const val TIMELINE_ENDPOINT = "1.1/statuses/user_timeline.json"

interface TimelineApiService {

    @POST(AUTH_ENDPOINT)
    fun getToken(@Header("Authorization") auth: String): Single<TwitterToken>

    @GET(TIMELINE_ENDPOINT)
    fun getUserTimeline(
        @Query("screen_name") username: String,
        @Query("count") count: Int,
        @Query("max_id") maxId: BigInteger?       // nullable
    ): Single<List<TimelineItemEntity>>
}