package com.chernenkovit.timeline.timeline.presentation.screen.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.base.presentation.adapter.holder.BaseHolder
import com.chernenkovit.timeline.R

@EpoxyModelClass
abstract class ProgressFullViewModel : EpoxyModelWithHolder<ProgressFullScreenHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_progress_bar_full_screen

    override fun bind(holder: ProgressFullScreenHolder) {
    }
}

//add and customize attributes if necessary
class ProgressFullScreenHolder : BaseHolder() {
}

